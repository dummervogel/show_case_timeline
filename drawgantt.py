import pandas as pd
# must run conda install -c plotly plotly first
import plotly.offline as py
import plotly.figure_factory as ff
import numpy as np

# load and format the data
df = pd.read_excel("ganttdata.xlsx", parse_cols="B:F")
df["Datetime"] = df["Datetime"].str.replace("/","-")
df["start_time"] = df["Datetime"] + " " + df["start_time"]
df["end_time"] = df["Datetime"] + " " + df["end_time"]

# process only 7 rows, rename the columns required by plotly
df2 = df.loc[0:6,["appID", "start_time", "end_time", "executeDBEnName"]]
df2.columns = ["Task", "Start", "Finish", "Resource"]

# add NULL bars
empty_tasks = [] 
for task, data in df2.groupby("Task"):
	temp_time = df2.loc[:,["Start","Finish"]].shift(1)
	for row1, row2 in zip(data.iterrows(), temp_time.iterrows()):
		if pd.isnull(row2[1]["Start"]) or row2[1]["Finish"] == row1[1]["Start"]:
			continue
		empty_tasks.append([row1[1]["Task"], row2[1]["Finish"], row1[1]["Start"], "NULL"])
for empty_task in empty_tasks:
	df2.loc[df2.shape[0]] = empty_task

# draw df2
colors = {i: tuple(np.random.rand(3).tolist()) if i != "NULL" else (0,1,1) for i in df2["Resource"].unique()}

fig = ff.create_gantt(df2, colors=colors, index_col = "Resource",show_colorbar=True, group_tasks=True)
py.plot(fig, filename="outgantt.html", auto_open=True)