import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_excel("examiner1_time.xlsx", sheet_name="gen_total")
fig,ax = plt.subplots()
ax.hist(df["total_time"], bins=[0,30,60,100,200])
ax.set_xlabel("total time (min)")
ax.set_ylabel("number")

plt.show()