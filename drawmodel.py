import pandas as pd
import re
import numpy as np
from itertools import accumulate
import matplotlib.pyplot as plt

df = pd.read_excel("modeldata.xlsx", parse_cols="B,D,J")
df.sort_values(["appID","time"],inplace=True)

# find each segment
y = [int(i) for i in (df["executeDBEnName"]!=df["executeDBEnName"].shift(1)) | (df["appID"]!=df["appID"].shift(1))]
df["inc"] = list(accumulate(y))

# determine start time and end time for each segment
gb = df.groupby("inc")
df2 = gb.agg({"appID": lambda x: x.iloc[0],
	"executeDBEnName": lambda x: x.iloc[0],
	"time": [np.min, np.max]})
df2.columns = [i[0] if i[0]!="time" else "_".join(i) for i in df2.columns.ravel()]

# add empty 

# convert time to integer
for i in ("time_amin", "time_amax"):
	y = [re.match(r"([0-9]*):([0-9]*):([0-9]*)", j) for j in df2[i]]
	df2[i + "_int"] = [int(j.group(1))*3600+int(j.group(2))*60+int(j.group(3)) for j in y]

df2["diff"] = df2["time_amax_int"] - df2["time_amin_int"]

# draw for each appID
fit,ax = plt.subplots()
apps = []
# generate colors for each database
lis_dbs = df2["executeDBEnName"].unique()
dic_colors = {i:np.random.rand(3).tolist() for i in lis_dbs}


# the interval of bar for the  app
interval = 4
height = 2
bottom = interval 

for app, data in df2.groupby("appID"):
	apps.append(app)
	print(data)
	# draw each bar
	for index,row in data.iterrows():
		print(bottom)
		ax.broken_barh([(row["time_amin_int"], row["diff"])], 
			(bottom, height), 
			facecolor=dic_colors[row["executeDBEnName"]])
		plt.text(row["time_amin_int"], bottom+height, row["time_amin"])
		plt.text(row["time_amin_int"], bottom, row["executeDBEnName"])
	# update the bottom of bar for each app
	bottom =bottom + height + interval
ax.set_xlabel("time")
ax.set_ylabel("appID")
ax.set_yticks(range(interval + height//2, (height+interval)*len(apps), height+interval))
ax.set_yticklabels(apps)
ax.grid(True)
plt.show()