import pandas as pd
import re
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import math
import pdb

# load and format the data
print("loading data")
df = pd.read_excel("ganttdata_test.xlsx", parse_cols="A:F")

userID = df.loc[0, "UserID"]
# 
df = df.loc[0:37,:]
#
for col in ("start_time", "end_time"):
	df[col] = df["Datetime"] + " " + df[col]
	df["dt_"+col] = [x.to_period(freq="S").ordinal for x in pd.to_datetime(df[col], format="%Y/%m/%d %H:%M:%S")]

df.sort_values(["appID","dt_start_time"],inplace=True)

gb = df.groupby("appID")
# get the minium date for each app
res = gb["Datetime"].agg(lambda x: x.iloc[0])
# calculate the relative value w.r.t earlest day for each appID
for appID in res.index:
	dt_start_day = pd.to_datetime(res[appID],format="%Y-%m-%d").to_period(freq="S").ordinal
	df.loc[df["appID"] == appID, "dt_start_time"] -= dt_start_day
	df.loc[df["appID"] == appID, "dt_end_time"] -= dt_start_day


# add NULL values
empty_tasks = [] 
for task, data in gb:
	temp_time = data.loc[:,["start_time", "end_time", "dt_start_time","dt_end_time"]].shift(1)
	for row1, row2 in zip(data.iterrows(), temp_time.iterrows()):
		if pd.isnull(row2[1]["dt_start_time"]) or row2[1]["dt_end_time"] == row1[1]["dt_start_time"]:
			continue
		empty_tasks.append([userID, row1[1]["appID"], None, row2[1]["end_time"], row1[1]["start_time"], "NULL",row2[1]["dt_end_time"], row1[1]["dt_start_time"]])

for empty_task in empty_tasks:
	df.loc[df.shape[0]] = empty_task
df["diff"] = df["dt_end_time"] - df["dt_start_time"]


# generate output results
# output should be 
# {app1: [[db1_name, db1_time],[db2_name,db2_time],[db3_name, db3_time]]
# ,app2: [[db1_name, db1_time], [db2_name, db2_time]]}
print("generating output")
output = {}
max_dbs = 0
for task, data in df[df["executeDBEnName"] != "NULL"].groupby("appID"):
	last_db = None
	output_task = []
	for row in data.iterrows():
		if last_db is None or last_db != row[1]["executeDBEnName"]:
			last_db = row[1]["executeDBEnName"]
			output_task += [[last_db, row[1]["diff"]]]
		else:
			output_task[-1][1] += row[1]["diff"]
	output[int(task)] = output_task
	if len(output_task) > max_dbs:
		max_dbs = len(output_task)
# convert to dataframe
db_indices = range(1, max_dbs+1)
col_db_names = ["db{0}_name".format(i) for i in db_indices]
col_db_times = ["db{0}_time".format(i) for i in db_indices]
col_dbs = [j for i in zip(col_db_names, col_db_times) for j in i]
output_df = pd.DataFrame(index=output.keys(), columns=col_dbs)
output_df.index.names = ["appID"]
for app, db_values in output.items():
	for db_value, i in zip(db_values, range(1, len(db_values)+1)):
		output_df.loc[app, "db{0}_name".format(i)] = db_value[0]
		output_df.loc[app, "db{0}_time".format(i)] = db_value[1]
output_df["total_time"] = output_df[col_db_times].sum(axis=1)
output_df.loc[:, col_db_times] = output_df.loc[:, col_db_times] // 60
output_df["total_time"] = output_df["total_time"] // 60
# generate jump times and back times
output_df["jump_times"] = 0
output_df["back_times"] = 0
for app, db_values in output.items():
	visited_db = set()
	jump_times = 0
	back_times = 0
	for db_value in db_values:
		if db_value[0] in visited_db:
			back_times += 1
		else:
			visited_db.add(db_value[0])
		jump_times += 1
	output_df.loc[app, "jump_times"] = jump_times
	output_df.loc[app, "back_times"] = back_times
# add other columns
output_df["userID"] = userID
output_df.reset_index(inplace=True)
output_df["appID"] = output_df["appID"].astype(str) # suppress e-notation in excel	
# write to excel
writer = pd.ExcelWriter('output.xlsx')
output_df.to_excel(writer, columns = ["userID", "appID", "total_time"] + col_dbs + ["jump_times", "back_times"], index=False)
writer.save()

# generate colors for each database
print("drawing pictures")
lis_dbs = df["executeDBEnName"].unique()
dic_colors = {}
color_template = [(1,0,0),(0,1,0),(0,0,1),(0,1,1),(1,1,0),(1,0,1)]
dic_colors = {db:color_template[color_index % len(color_template)]
	for db,color_index in zip(lis_dbs, range(0, len(lis_dbs)))}


# the interval of bar for the  app
interval = 4
height = 2
bottom = interval
apps = []
bars = []
bar_descriptions = []
# start drawing for each app
fig,ax = plt.subplots()
for app, data in df.groupby("appID"):
	apps.append(int(app))
	#print(data)
	# draw each bar
	for index,row in data.iterrows():
		#print(bottom)
		bars.append(ax.broken_barh([(row["dt_start_time"], row["diff"])], 
			(bottom, height), 
			facecolor=dic_colors[row["executeDBEnName"]]))
		bar_descriptions.append((row["dt_start_time"],
			bottom+height/2,
			"{0}->\n{1}\n in {2}".format(row["start_time"],row["end_time"],row["executeDBEnName"]) ) )
		#plt.text(row["dt_start_time"], bottom+height, row["start_time"])
		#plt.text(row["dt_start_time"], bottom, row["executeDBEnName"], fontsize=6)
	# update the bottom of bar for each app
	bottom =bottom + height + interval

seconds_per_day = 86400
xrange = range(seconds_per_day, math.ceil(df["dt_end_time"].max()/seconds_per_day) * seconds_per_day, seconds_per_day)
ax.set_xticks(xrange)
ax.set_xticklabels(["Day{0}".format(i//seconds_per_day) for i in xrange])
ax.set_ylabel("appID")
ax.set_yticks(range(interval + height//2, (height+interval)*len(apps), height+interval))
ax.set_yticklabels(apps)
ax.spines['top'].set_visible(False)
ax.spines['bottom'].set_visible(False)
ax.spines['right'].set_visible(False)
patches = [mpatches.Patch(color=color, label=app) for app, color in dic_colors.items()]
plt.legend(handles=patches,bbox_to_anchor=(1, 1), loc=2)
ax.xaxis.grid(True)


annot = ax.annotate("", xy=(0,0), xytext=(-20,20),textcoords="offset points",
	arrowprops=dict(arrowstyle="->"))
annot.set_visible(False)

# attach the hover event
def hover(e):
	vis = annot.get_visible()
	if e.inaxes == ax:
		for bar, bar_description in zip(bars, bar_descriptions):
			cont, ind = bar.contains(e)
			if cont:
				annot.xy = (bar_description[0], bar_description[1])
				annot.set_text(bar_description[2])
				annot.set_visible(True)
				fig.canvas.draw_idle()
				return
	if vis:
		annot.set_visible(False)
		fig.canvas.draw_idle()
		

fig.canvas.mpl_connect("motion_notify_event", hover)
plt.show()