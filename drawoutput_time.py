import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import re
import matplotlib.patches as mpatches

import pdb

df = pd.read_excel("examiner1_time.xlsx", index_col="appID")
# create a 3*1 plot
fig,ax = plt.subplots(3,1)

# draw bar for total_time
x_pos = np.arange(len(df.index))
ax[0].bar(x_pos, df["total_time"])
ax[0].get_xaxis().set_ticks([])
ax[0].set_xlabel("apps")
ax[0].set_ylabel("total time (min)")

# draw percentage bar
col_db_names = []
col_db_times = []
for col in df.columns:
    if col.startswith("db"):
        if col.endswith("_time"):
            col_db_times += [col]
        elif col.endswith("_name"):
            col_db_names += [col]
# get all db_names，set color for each db
db_names = df[col_db_names].stack().unique()
dic_colors = {}
color_template = [(1,0,0),(0,1,0),(0,0,1),(0,1,1),(1,1,0),(1,0,1)]
dic_colors = {db:color_template[color_index % len(color_template)]
	for db,color_index in zip(db_names, range(0, len(db_names)))}
# plot bars
total_time = df[col_db_times].sum()
x_pos = np.arange(len(col_db_names))
bottom = np.zeros(len(col_db_names))
for db_name in db_names:
	result = [df.loc[df[col_db_name]==db_name, col_db_time].sum() 
		for col_db_name, col_db_time in zip(col_db_names, col_db_times)]
	# calculate the percentage
	result = result / total_time * 100
	# draw one row of bars
	ax[1].bar(x_pos, result, bottom = bottom, facecolor = dic_colors[db_name])
	# update the bottom of bars
	bottom += result

ax[1].set_xticks(x_pos)
ax[1].set_xticklabels(["db{0}".format(i+1) for i in range(0, len(col_db_names))])
ax[1].set_ylabel("percentage of time in dbs(%)")
patches = [mpatches.Patch(color=color, label=db_name) for db_name, color in dic_colors.items()]
ax[1].legend(handles=patches,bbox_to_anchor=(1, 1), loc=2)

# draw histogram for total time
num_bins = 8
ax[2].hist(df["total_time"], num_bins, density = True)
ax[2].set_xlabel("total time (min)")
ax[2].set_ylabel("probability")
plt.show()
